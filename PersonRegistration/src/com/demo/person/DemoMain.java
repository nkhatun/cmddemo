package com.demo.person;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.demo.person.domain.Person;
import com.demo.person.dto.ResponseDTO;
import com.demo.person.service.PersonService;
import com.demo.person.service.PersonServiceImpl;

public class DemoMain {
	private static PersonService personServiceImpl = new PersonServiceImpl();

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		try {
		String userInput;
		boolean exit = false;
		do {
			System.out.print("****************Menu Starts****************** \n");
			System.out.print("*. Press 1 To Fetch All Persons List \n");
			System.out.print("*. Press 2 To Fetch Persons Details \n");
			System.out.print("*. Press 3 To Delete Person \n");
			System.out.print("*. Press 4 To Add Person \n");
			System.out.print("*. Press 5 To Exit \n");
			System.out.print("****************Menu Ends****************** \n\n");

			System.out.println("Enter your choice:");
			userInput = scanner.next();

			switch (userInput) {
			case "1":
				Person[] personList = personServiceImpl.getAllPersons();
				if (personList != null && personList.length != 0) {
					System.out.println("Person Details Below : ");
					for (Person p : personList) {
						System.out.println("\n");
						System.out.println("ID : " + p.getId());
						System.out.println("First Name : " + p.getFirst_name());
						System.out.println("Last name : " + p.getLast_name());
						System.out.println("Age : " + p.getAge());
						System.out.println("Favourite Colour : " + p.getFavourite_colour());
						System.out.println("Hobbies : " + String.join(",", p.getHobby()));
						System.out.println("\n");

					}
				} else {
					System.out.println("No Person Registered Yet");
					System.out.println("\n");
				}
				break;
			case "2":
				System.out.println("Enter Person Id To Fetch Details:");
				boolean idFlag;
				do {
					// must be a digit from 0 - 9
					String digit = "\\d";
					String idInput = scanner.next();
					idFlag = idInput.matches(digit);
					if (!idFlag) {
						System.out.println("Please Enter A Number Value!");
					} else {
						int id = Integer.valueOf(idInput);
						Person p = personServiceImpl.getPerson(id);
						if (p != null) {
							System.out.println("\n");
							System.out.println("First Name : " + p.getFirst_name());
							System.out.println("Last name : " + p.getLast_name());
							System.out.println("Age : " + p.getAge());
							System.out.println("Favourite Colour : " + p.getFavourite_colour());
							System.out.println("Hobbies : " + String.join(",", p.getHobby()));
							System.out.println("\n");

						} else {
							System.out.println("\n");
							System.out.println("No Person Details Found With ID: " + id);
							System.out.println("\n");
						}
					}
				} while (!idFlag);
				break;
			case "3":
				System.out.println("Enter Person Id To Delete Person:");
				boolean deleteFlag;
				do {
					// must be a digit from 0 - 9
					String digit = "\\d";
					String idInput = scanner.next();
					deleteFlag = idInput.matches(digit);
					if (!deleteFlag) {
						System.out.println("Please Enter A Number Value!");
					} else {
						int id = Integer.valueOf(idInput);
						ResponseDTO response = personServiceImpl.deletePerson(id);
						System.out.println("\n");
						System.out.println(response.getMessage());
						System.out.println("\n");
					}
				} while (!deleteFlag);

				break;
			case "4":
				Person person = new Person();
				System.out.println("Enter First Name:");
				String firstname = scanner.nextLine(); // Read the first name
				while (!firstname.matches("[a-zA-Z]+")) { // Check if it has anything other than alphabets
					System.out.println("Please Enter Single And Valid Input For First Name");
					firstname = scanner.nextLine(); // if not, ask the user to enter new first name
				}
				person.setFirst_name(firstname);
				
				System.out.println("Enter Last Name:");
				String lastName = scanner.nextLine();
				while (!lastName.matches("[a-zA-Z_]+")) {
					System.out.println("Please Enter Single And Valid Input For Last Name");
					lastName = scanner.nextLine();
				}
				person.setLast_name(lastName);
				
				System.out.println("Enter Age:");
				boolean flag;
				do {
					String digit = "\\d{1,2}";
					String ageInput = scanner.nextLine();
					flag = ageInput.matches(digit);
					if (!flag) {
						System.out.println("Please Enter Age Between 1 To 99!");
					} else {
						int age = Integer.valueOf(ageInput);
						person.setAge(age);
					}
				} while (!flag);

				System.out.println("Enter Favourite Colour:");
				String favColor = scanner.nextLine();
				person.setFavourite_colour(favColor);

				System.out.println("Enter Hobbies:");
				System.out.println("Enter 0 To Exit Hobby Add Loop");
				List<String> hobbyList = new ArrayList<String>();
				String input = scanner.nextLine();
				hobbyList.add(input);
				while (!input.equals("0")) {
					input = scanner.nextLine();
					hobbyList.add(input);

				}
				hobbyList.remove("0");
				person.setHobby(hobbyList);
				
				ResponseDTO response = personServiceImpl.addPerson(person);
				System.out.println(response.getMessage());
				System.out.println("\n");
				break;
			case "5":
				// exit from the program
				exit = true;
				System.out.println("Exiting...");
				System.exit(0);
			default:
				// inform user in case of invalid choice.
				System.out.println("Invalid choice. Read the options carefully...");
				System.out.println("\n");
			}
		} while (!exit);
		}
		finally {
			scanner.close();
		}
	}

}
