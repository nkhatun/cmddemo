package com.demo.person.service;

import com.demo.person.domain.Person;
import com.demo.person.dto.ResponseDTO;

public interface PersonService {
	public ResponseDTO addPerson(Person p);
	public ResponseDTO deletePerson(int id);
	public Person getPerson(int id);
	public Person[] getAllPersons();
}
