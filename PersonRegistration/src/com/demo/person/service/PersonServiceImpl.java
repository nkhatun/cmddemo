package com.demo.person.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.demo.person.domain.Person;
import com.demo.person.dto.ResponseDTO;

public class PersonServiceImpl implements PersonService{
	private static Map<Integer,Person> persons = new HashMap<Integer,Person>();

	public ResponseDTO addPerson(Person p) {
		ResponseDTO response = new ResponseDTO();
		if(persons.get(p.getId()) != null){
			response.setStatus(false);
			response.setMessage("Person Already Exists");
			return response;
		}
		int id = createPersonId();
		p.setId(id);
		persons.put(id, p);
		response.setStatus(true);
		response.setMessage("Person created successfully");
		return response;
	}

public int createPersonId() {
	if(persons.isEmpty()) {return 1;}
    int maxValueInMap=(Collections.max(persons.keySet()));  // This will return max value in the Hashmap
	return maxValueInMap+1;
}
	@Override
	public ResponseDTO deletePerson(int id) {
		ResponseDTO response = new ResponseDTO();
		if(persons.get(id) == null){
			response.setStatus(false);
			response.setMessage("Person Doesn't Exists");
			return response;
		}
		persons.remove(id);
		response.setStatus(true);
		response.setMessage("Person deleted successfully");
		return response;
	}

	@Override
	public Person getPerson(int id) {
		return persons.get(id);
	}

	@Override
	public Person[] getAllPersons() {
		if(!persons.isEmpty()) {
		Set<Integer> ids = persons.keySet();
		Person[] p = new Person[ids.size()];
		int i=0;
		for(Integer id : ids){
			p[i] = persons.get(id);
			i++;
		}
		
		return p;
		}
		return null;
	}

}
